<?php
/*
 Template Name: Homepage
 */

include("homepageheader.php"); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>
        
	<?php endwhile; // end of the loop. ?>
    
</body>
</html>