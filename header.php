<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package wp-synergygaming
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link href='http://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,700,900|Roboto+Condensed:400,700' rel='stylesheet'>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="site__header" role="banner">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site__title" title="Home">synergy <span>gaming</span></a>

		<nav id="site-navigation" class="main-nav" role="navigation">

			<a class="main-nav__controller--open" href="#masthead" title="Open Menu">menu</a>
      <a class="main-nav__controller--close" href="#0" title="Close Menu">menu</a>

			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
