<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package wp-synergygaming
 */
?>

<article id="post-<?php the_ID(); ?>">
	<header class="page__header">
		<h1 class="page__title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="page__wrapper">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wp-synergygaming' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .page__wrapper -->
</article><!-- #post-## -->
